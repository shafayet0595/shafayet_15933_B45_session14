<?php
namespace App;


class Calcualtor
{
    private  $num1;
    private  $num2;
    private  $operation;
    private $result;

    public function setNum1($num1)
    {
        $this->num1 = $num1;
    }
    public function getNum1()
    {
        return $this->num1;
    }

    public function setNum2($num2)
    {
        $this->num2 = $num2;
    }

    public function getNum2()
    {
        return $this->num2;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    public function setResult()
    {
        switch($this->operation){
            case "Addition":
                $this->result = $this->doAddition();
                break;
            case "Subtraction":
                $this->result = $this->doSubtraction();
                break;
            case "Multiplication":
                $this->result = $this->doMultiplication();
                break;
            case "Division":
                $this->result = $this->doDivision();
                break;
        }

    }
    public function getResult()
    {
        $this->setResult();
        return $this->result;
    }
    private function doAddition(){
        return $this->num1 + $this ->num2;
    }
    private function doSubtraction(){
        return $this->num1 - $this ->num2;
    }
    private function doMultiplication(){
        return $this->num1 * $this ->num2;
    }
    private function doDivision(){
        return $this->num1 / $this ->num2;
    }
}